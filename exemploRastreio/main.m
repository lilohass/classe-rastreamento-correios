//
//  main.m
//  exemploRastreio
//
//  Created by William Hass on 6/7/14.
//  Copyright (c) 2014 William Hass. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
