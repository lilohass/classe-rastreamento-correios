//
//  CorreiosWS.m
//  cade2
//
//  Created by William Hass on 5/14/14.
//  Copyright (c) 2014 William Hass. All rights reserved.
//

#import "CorreiosWS.h"
#import "XMLParser.h"

@implementation CorreiosWS {
    NSMutableData *_responseData;
    NSMutableArray *_encomendas;
    NSMutableArray *_grupoRequisicao;
    int currentGroup;
}

static NSString *PARAM_USUARIO                              = @"Usuario";
static NSString *PARAM_SENHA                                = @"Senha";
static NSString *PARAM_RESULTADO                            = @"Resultado";
static NSString *PARAM_TIPO                                 = @"Tipo";
static NSString *PARAM_OBJETOS                              = @"Objetos";
static NSString *DEF_CONTENT_TYPE                           = @"application/x-form-urlencoded";
static NSString *HTTP_METHOD                                = @"POST";
static NSString *IDENT                                      = @"correios";
static NSString *URL_WS                                     = @"http://websro.correios.com.br/sro_bin/sroii_xml.eventos";
static NSString *DEF_USUARIO                                = @"ECT";
static NSString *DEF_SENHA                                  = @"SRO";
static int DEF_TEMPO_LIMITE                                 = 10;
static int MAX_ENCOMENDAS_POR_REQ                           = 5;
static ResultadoCorreios DEF_RESULTADO                      = ResultadoCorreiosTodos;
static TipoResultadoCorreios DEF_TIPO_RESULTADO_CORREIOS    = TipoResultadoCorreiosLista;
static NSString *RESULTADO_TODOS                            = @"T";
static NSString *RESULTADO_ULTIMO                           = @"U";
static NSString *TIPO_RESULTADO_LISTA                       = @"L";
static NSString *TIPO_RESULTADO_INTERVALO                   = @"F";
static NSString *OBJETOS_KEY                                = @"objeto";
static NSString *EVENTOS_KEY                                = @"evento";
static NSString *ERROR_KEY                                  = @"error";

- (id) init
{
    self = [super init];
    
    if(self) {
        tempoLimite             = DEF_TEMPO_LIMITE;
        usuario                 = DEF_USUARIO;
        senha                   = DEF_SENHA;
        resultado               = DEF_RESULTADO;
        tipoResultadoCorreios   = DEF_TIPO_RESULTADO_CORREIOS;
    }
    
    return self;
}



- (BOOL)encomendaValida:(Encomenda *)enc
{
    return enc != nil && enc.companhia != nil && enc.companhia.ident == IDENT;
}



- (BOOL)addEncomenda:(Encomenda *)enc
{
    if(![self encomendaValida:enc])
        return NO;
    
    if(!_encomendas)
        _encomendas = [[NSMutableArray alloc] init];
    
    [_encomendas addObject:enc];
    return YES;
}




- (void)rastrear
{
    [self montaGrupoRequisicao];
    
    currentGroup = 0;
    [self disparaRequisicaoGrupo:currentGroup];
}



- (void)disparaRequisicaoGrupo:(int)grupo
{
    NSMutableString *strObjetos = [[NSMutableString alloc] init];
    for(Encomenda *enc in [_grupoRequisicao objectAtIndex:grupo])
        [strObjetos appendString:[enc.codRastreio uppercaseString]];
    
    NSData *data = [self buildRequestDataForObjects:strObjetos];
    NSURLConnection *conn = [[NSURLConnection alloc] initWithRequest:[self getRequestWithData:data] delegate:self];
    [conn start];
}


- (void) montaGrupoRequisicao
{
    int cont = 0;
    _grupoRequisicao = [[NSMutableArray alloc] init];
    
    for(int i=0; i < _encomendas.count; i++) {
        
        int lastKey = (int)_grupoRequisicao.count-1;
        
        if(cont > lastKey)
            [_grupoRequisicao addObject:[[NSMutableArray alloc] init]];
        
        [[_grupoRequisicao objectAtIndex:cont] addObject:_encomendas[i]];
        
        if((i+1) % MAX_ENCOMENDAS_POR_REQ == 0)
            cont++;
    }
}


- (NSURLRequest *)getRequestWithData:(NSData *)data
{
    NSMutableURLRequest *req = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:URL_WS] cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:tempoLimite];
    
    [req setHTTPMethod:HTTP_METHOD];
    [req setValue:DEF_CONTENT_TYPE forHTTPHeaderField:@"Content-Type"];
    [req setHTTPBody:data];
    
    return req;
}


- (NSData *)buildRequestDataForObjects:(NSString *)strObjetos
{
    NSString *strResultado, *strTipoResultado;
    
    strTipoResultado    = [self getStrTipoResultado:tipoResultadoCorreios];
    strResultado        = [self getStrResultado:resultado];
    
    NSString *strRet = [NSString stringWithFormat:@"%@=%@&%@=%@&%@=%@&%@=%@&%@=%@", PARAM_USUARIO,usuario,
                                                                                    PARAM_SENHA,senha,
                                                                                    PARAM_TIPO,strTipoResultado,
                                                                                    PARAM_RESULTADO,strResultado,
                                                                                    PARAM_OBJETOS, strObjetos];

    return [strRet dataUsingEncoding:NSStringEncodingConversionAllowLossy];
}



- (NSString *)getStrResultado:(ResultadoCorreios)result
{
    NSString *ret;
    
    switch (result) {
        case ResultadoCorreiosTodos:
            ret = RESULTADO_TODOS;
            break;
            
        case ResultadoCorreiosUltimo:
            ret = RESULTADO_ULTIMO;
            break;
    }
    
    return ret;
}

- (NSString *)getStrTipoResultado:(TipoResultadoCorreios)result
{
    NSString *ret;
    switch (resultado) {
        case TipoResultadoCorreiosLista:
            ret = TIPO_RESULTADO_LISTA;
            break;
        case TipoResultadoCorreiosIntervalo:
            ret = TIPO_RESULTADO_INTERVALO;
            break;
    }
    return ret;
}


- (TreeNode *)parseResultWithData:(NSData *)response
{
    XMLParser *parser   = [[XMLParser alloc] init];
    return [parser parseXMLFromData:response];
}


#pragma mark
- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    //[self avisaDelegateFalha:CorreiosErroFalhaConexao];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [_responseData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    _responseData = [[NSMutableData alloc] init];
}


- (Evento *)eventoWithTree:(TreeNode *)event
{
    
    Evento *e = [[Evento alloc] init];
    
    NSString *strData = [event leavesForKey:@"data"].count > 0 ? [event leavesForKey:@"data"][0] : nil;
    NSString *strHora = [event leavesForKey:@"hora"].count > 0 ? [event leavesForKey:@"hora"][0] : nil;
    
    e.data      = strData && strHora ? [[NSDate dataComData:strData eHora:strHora] timeIntervalSince1970] : 0;
    
    e.descricao = [event leavesForKey:@"descricao"].count > 0 ? [event leavesForKey:@"descricao"][0] : @"";
    e.local     = [event leavesForKey:@"local"].count > 0 ?     [event leavesForKey:@"local"][0] : @"";
    e.cidade    = [event leavesForKey:@"cidade"].count > 0 ?    [event leavesForKey:@"cidade"][0] : @"";
    e.estado    = [event leavesForKey:@"uf"].count > 0 ?        [event leavesForKey:@"uf"][0] : @"";
    
    return e;
}


- (void)avisaDelegateAcabou
{
    if([self.delegate respondsToSelector:@selector(rastreamentoCorreiosAcabou:)])
        [self.delegate performSelector:@selector(rastreamentoCorreiosAcabou:) withObject:_encomendas];
}


- (void)avisaDelegateSucesso:(Encomenda *)encomenda
{
    if([self.delegate respondsToSelector:@selector(rastreamentoCorreiosSucesso:)])
        [self.delegate performSelector:@selector(rastreamentoCorreiosSucesso:) withObject:encomenda];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    int lastKey = (int)_grupoRequisicao.count-1;
    
    if(_responseData == nil || [_responseData length] == 0) {
        /* TODO TRATAR FALHA */
    } else {
        
        TreeNode *tree = [self parseResultWithData:_responseData];
        
        NSMutableArray *objetos = [tree nodesForKey:OBJETOS_KEY];
        NSUInteger qtdObjetos = objetos.count;
        
        if([[tree leavesForKey:ERROR_KEY] count] > 0 || qtdObjetos == 0) {
            /* TODO AVISAR FALHA */
        } else {
            
            NSMutableArray *grupo = [_grupoRequisicao objectAtIndex:currentGroup];
            
            for(NSUInteger indexObjeto = 0; indexObjeto < qtdObjetos; indexObjeto++) {
                
                Encomenda *enc = [grupo objectAtIndex:indexObjeto];
                
                TreeNode *node = objetos[indexObjeto];
                
                NSMutableArray *eventosXML = [node nodesForKey:EVENTOS_KEY];
                
                NSMutableArray *eventosEncomenda = [[NSMutableArray alloc] init];
                for(int i =0; i < eventosXML.count; i++) {
                    TreeNode *event = eventosXML[i];
                    Evento *e = [self eventoWithTree:event];
                    [eventosEncomenda addObject:e];
                }
                
                [enc setEventosWithArray:eventosEncomenda];
                
                [self avisaDelegateSucesso:enc];
            }
            
        }
        
    }
    
    
    currentGroup++;
    if(lastKey >= currentGroup) {
        [self disparaRequisicaoGrupo:currentGroup];
    } else {
        [self avisaDelegateAcabou];
    }
    
}

@end
