//
//  NSDate+NSDateCategory.m
//  cade2
//
//  Created by William Hass on 5/15/14.
//  Copyright (c) 2014 William Hass. All rights reserved.
//

#import "NSDate+NSDateCategory.h"

@implementation NSDate (NSDateCategory)

+ (NSDate *)dataComData:(NSString *)data eHora:(NSString *)hora
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd/MM/yyyy HH:mm"];
    
    return [formatter dateFromString:[NSString stringWithFormat:@"%@ %@",data,hora]];
}

@end
