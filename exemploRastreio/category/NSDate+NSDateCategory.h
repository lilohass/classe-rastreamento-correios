//
//  NSDate+NSDateCategory.h
//  cade2
//
//  Created by William Hass on 5/15/14.
//  Copyright (c) 2014 William Hass. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (NSDateCategory)

+ (NSDate *)dataComData:(NSString *)data eHora:(NSString *)hora;

@end
