//
//  ViewController.m
//  exemploRastreio
//
//  Created by William Hass on 6/7/14.
//  Copyright (c) 2014 William Hass. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    CorreiosWS *ws = [[CorreiosWS alloc] init];
    
    Encomenda *teste = [[Encomenda alloc] init];
    teste.codRastreio = @"LJ936346710US";
    teste.companhia = [[Companhia alloc] init];
    teste.companhia.ident = @"correios";
    
    Encomenda *teste2 = [[Encomenda alloc] init];
    teste2.codRastreio = @"PD672655429BR";
    teste2.companhia = [[Companhia alloc] init];
    teste2.companhia.ident = @"correios";
    
    Encomenda *teste3 = [[Encomenda alloc] init];
    teste3.codRastreio = @"SW887779164BR";
    teste3.companhia = [[Companhia alloc] init];
    teste3.companhia.ident = @"correios";
    
    Encomenda *teste4 = [[Encomenda alloc] init];
    teste4.codRastreio = @"SW887778739BR";
    teste4.companhia = [[Companhia alloc] init];
    teste4.companhia.ident = @"correios";
    
    Encomenda *teste5 = [[Encomenda alloc] init];
    teste5.codRastreio = @"SW887778858BR";
    teste5.companhia = [[Companhia alloc] init];
    teste5.companhia.ident = @"correios";
    
    Encomenda *teste6 = [[Encomenda alloc] init];
    teste6.codRastreio = @"SW686563585BR";
    teste6.companhia = [[Companhia alloc] init];
    teste6.companhia.ident = @"correios";
    
    Encomenda *teste7 = [[Encomenda alloc] init];
    teste7.codRastreio = @"DG296105986BR";
    teste7.companhia = [[Companhia alloc] init];
    teste7.companhia.ident = @"correios";
    
    [ws addEncomenda:teste];
    [ws addEncomenda:teste2];
    [ws addEncomenda:teste3];
    [ws addEncomenda:teste4];
    [ws addEncomenda:teste5];
    [ws addEncomenda:teste6];
    
    [ws  setDelegate:self];
    
    [ws rastrear];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark Correios WS Delegate
- (void) rastreamentoCorreiosAcabou:(NSArray *)encomendas
{
    for (int i = 0; i < encomendas.count; i++) {
        Encomenda *enc = [encomendas objectAtIndex:i];
        
        NSLog(@" ----- Encomenda: %@ ----- ", enc.codRastreio);
        
        NSLog(@"%@",[enc getEventos]);
        
        NSLog(@" ------------------------- ");
        
    }
}

@end
