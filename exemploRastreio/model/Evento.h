//
//  Evento.h
//  cade2
//
//  Created by William Hass on 3/29/14.
//  Copyright (c) 2014 William Hass. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Evento : NSObject

@property NSInteger idEvento;
@property NSString *descricao;
@property NSTimeInterval data;
@property NSString *pais;
@property NSString *estado;
@property NSString *cidade;
@property NSString *cep;
@property NSString *local;
@end
