//
//  Evento.m
//  cade2
//
//  Created by William Hass on 3/29/14.
//  Copyright (c) 2014 William Hass. All rights reserved.
//

#import "Evento.h"

@implementation Evento

- (NSString *)description
{
    return [NSString stringWithFormat:  @"{\r idEvento : %d \r"
                                        @" descricao: %@ \r"
                                        @" data: %@ \r"
                                        @" pais : %@ \r"
                                        @" estado: %@ \r"
                                        @" cidade: %@ \r"
                                        @" cep: %@ \r"
                                        @" local: %@ \r} ",(int)self.idEvento, self.descricao, [NSDate dateWithTimeIntervalSince1970:self.data], self.pais, self.estado, self.cidade, self.cep, self.local];
}

@end
