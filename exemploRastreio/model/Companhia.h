//
//  Companhia.h
//  cade2
//
//  Created by William Hass on 3/29/14.
//  Copyright (c) 2014 William Hass. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Companhia : NSObject

@property NSInteger idCompanhia;
@property NSString *nome;
@property NSArray *encomendas;
@property NSString *ident;

@end
