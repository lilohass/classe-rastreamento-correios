//
//  Encomenda.h
//  cade2
//
//  Created by William Hass on 3/29/14.
//  Copyright (c) 2014 William Hass. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Evento.h"
#import "Companhia.h"

@interface Encomenda : NSObject {
    NSMutableArray *eventos;
}

@property NSInteger idEncomenda;
@property NSString *codRastreio;
@property NSString *descricao;
@property NSInteger dataCadastro;
@property NSInteger ultimaAtualizacao;
@property BOOL recebeNotificacoes;
@property Companhia *companhia;

- (BOOL)valida;
- (NSString *)getErrorMessage;
- (UIAlertView *)getErrorAlert;
- (void)addEvento:(Evento *)ev;
- (NSArray *)getEventos;
- (void)setEventosWithArray:(NSArray *)evnts;

@end
