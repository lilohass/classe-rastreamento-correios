//
//  Encomenda.m
//  cade2
//
//  Created by William Hass on 3/29/14.
//  Copyright (c) 2014 William Hass. All rights reserved.
//

#import "Encomenda.h"

@implementation Encomenda

- (BOOL)valida
{
    if(self.companhia == nil || self.codRastreio == nil || self.codRastreio.length <= 0)
        return NO;
    else
        return YES;
}



- (NSString *)getErrorMessage
{
    NSMutableString *message = [NSMutableString stringWithString:NSLocalizedString(@"add_tracking_error_message", nil)];
    
    if(self.codRastreio.length <= 0)
        [message appendFormat:@"\n%@",NSLocalizedString(@"add_tracking_error_message_description", nil)];
    
    if(self.companhia == nil)
        [message appendFormat:@"\n%@",NSLocalizedString(@"add_tracking_error_message_courier", nil)];
    
    return (NSString *)message;
}


- (UIAlertView *)getErrorAlert
{
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"add_tracking_error_title", nil)
                                                    message:[self getErrorMessage]
                                                   delegate:self
                                          cancelButtonTitle:NSLocalizedString(@"add_tracking_error_button", nil)
                                          otherButtonTitles: nil];
    return alert;
}


- (void)addEvento:(Evento *)ev
{
    if(ev == nil)
        return;
    
    if(eventos == nil)
        eventos = [[NSMutableArray alloc] init];
    
    [eventos addObject:ev];
}

- (void)setEventosWithArray:(NSArray *)evnts
{
    eventos = [NSMutableArray arrayWithArray:evnts];
}


- (NSArray *)getEventos
{
    return (NSArray *)eventos;
}

@end
