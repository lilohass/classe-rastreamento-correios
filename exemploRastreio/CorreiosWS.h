//
//  CorreiosWS.h
//  cade2
//
//  Created by William Hass on 5/14/14.
//  Copyright (c) 2014 William Hass. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Encomenda.h"

typedef enum  {
    CorreiosErroCompanhiaInvalida,
    CorreiosErroFalhaConexao,
    CorreiosErroObjetoNaoEncontrado
} CorreiosErro;

typedef enum {
    ResultadoCorreiosTodos,
    ResultadoCorreiosUltimo
} ResultadoCorreios;

typedef enum {
    TipoResultadoCorreiosLista,
    TipoResultadoCorreiosIntervalo
} TipoResultadoCorreios;


@protocol CorreiosWSDelegate <NSObject>

@optional

- (void) rastreamentoCorreiosAcabou:(NSArray *)encomendas;
- (void) rastreamentoCorreiosSucesso:(Encomenda *)encomenda;

@end

@interface CorreiosWS : NSObject <NSURLConnectionDelegate>
{
    int tempoLimite;
    NSString *usuario;
    NSString *senha;
    ResultadoCorreios resultado;
    TipoResultadoCorreios tipoResultadoCorreios;
}

@property id<CorreiosWSDelegate> delegate;

- (void)rastrear;
- (BOOL)encomendaValida:(Encomenda *)enc;
- (BOOL)addEncomenda:(Encomenda *)enc;

@end
