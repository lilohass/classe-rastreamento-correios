//
//  ViewController.h
//  exemploRastreio
//
//  Created by William Hass on 6/7/14.
//  Copyright (c) 2014 William Hass. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CorreiosWS.h"

@interface ViewController : UIViewController <CorreiosWSDelegate>

@end
